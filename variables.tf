variable "name" {
  type        = string
  description = "Name of the application"
  default     = "myapp"
}

variable "environment_name" {
  type        = string
  description = "Current environment"
  default     = "development"
}
